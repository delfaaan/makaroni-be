const auth = {};

auth.validateUser = async (req, res, next) => {
	try {
		if (req.headers.authorization != 'makaronienduls') throw new Error(`You are not allowed in here.`);

		next();
	} catch (error) {
		return res.status(400).json({code: `02`, status: false, data: `Error occurred, reason: ${error.message}`});
	}
};

module.exports = auth;