const Sequelize = require('sequelize');
require('dotenv').config();

const sequelize = new Sequelize(
	process.env.DB_NAME,
	process.env.DB_USER,
	process.env.DB_PASS,
	{
		host: process.env.DB_HOST,
		port: process.env.DB_PORT,
		dialect: process.env.DB_DIALECT,
		logging: false
	}
);

sequelize.authenticate().then(() => {
	console.log(`Connecting to database is success.`);
}).catch((error) => {
	console.log(`Connecting to database is failed, reason: ${error.message}`);
});

module.exports = sequelize;