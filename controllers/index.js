const controller = {};
const service = require('../service');

controller.root = async (req, res) => {
	try {
		const { name } = req.query;

		const result = await service.root(name);

		return res.status(200).json({ code: `01`, status: true, data: result });
	} catch (error) {
		return res.status(400).json({ code: `02`, status: false, data: `Error occurred, reason: ${error.message}` });
	}
};

controller.getUsers = async (req, res) => {
	try {
		const result = await service.getUsers();

		return res.status(200).json({ code: `01`, status: true, data: result });
	} catch (error) {
		return res.status(400).json({ code: `02`, status: false, data: `Error occurred, reason: ${error.message}` });
	}
};

controller.getUser = async (req, res) => {
	try {
		const { id } = req.query;

		const result = await service.getUser(id);

		return res.status(200).json({ code: `01`, status: true, data: result });
	} catch (error) {
		return res.status(400).json({ code: `02`, status: false, data: `Error occurred, reason: ${error.message}` });
	}
};

controller.register = async (req, res) => {
	try {
		const payload = req.body;

		const result = await service.register(payload);

		return res.status(200).json({ code: `01`, status: true, data: result });
	} catch (error) {
		return res.status(400).json({ code: `02`, status: false, data: `Error occurred, reason: ${error.message}` });
	}
};

controller.login = async (req, res) => {
	try {
		const { email, password } = req.body;

		const result = await service.login(email, password);

		return res.status(200).json({ code: `01`, status: true, data: result });
	} catch (error) {
		return res.status(400).json({ code: `02`, status: false, data: `Error occurred, reason: ${error.message}` });
	}
};

module.exports = controller;