const service = {};
const model = require('../config/model');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
require('dotenv').config();

service.root = async (name) => {
	if (!name) throw new Error(`Name is required.`);

	return `Hello, ${name}!`;
};

service.getUsers = async () => {
	const result = await model.users.findAll();

	return result;
};

service.getUser = async (id) => {
	if (!id) throw new Error(`Parameter ID is required.`);

	const result = await model.users.findOne({ where: { id: id } });

	if (!result) throw new Error(`User with that ID could be not found.`);

	return result;
};

service.register = async (payload) => {
	const result = await model.users.findAll({ where: { email: payload.email } });

	if (result.length > 0) throw new Error(`User with this email has already been registered.`);

	const hashed = await bcrypt.hash(payload.password, 10);

	const newPayload = {
		username: payload.username,
		password: hashed,
		email: payload.email
	};

	const postUser = await model.users.create(newPayload);

	const token = jwt.sign({ userId: postUser.id }, process.env.JWT_TOKEN, { expiresIn: '2h' });

	return {
		message: `User has been registered successfully.`,
		token: token
	};
};

service.login = async (email, password) => {
	const result = await model.users.findOne({ where: { email } });

	if (!result || !bcrypt.compareSync(password, result.password)) throw new Error(`Invalid username or password.`);

	const token = jwt.sign({ userId: result.id }, process.env.JWT_TOKEN, { expiresIn: '2h' });

	return {
		message: `You logged in successfully.`,
		token: token
	};
};

module.exports = service;