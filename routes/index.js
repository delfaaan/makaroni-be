const express = require('express');
const router = express.Router();
const controller = require('../controllers');
const auth = require('../controllers/authorization');

router.get('/', auth.validateUser, controller.root);
router.get('/getUsers', auth.validateUser, controller.getUsers);
router.get('/getUser', auth.validateUser, controller.getUser);
router.post('/register', auth.validateUser, controller.register);
router.post('/login', auth.validateUser, controller.login);

module.exports = router;