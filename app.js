const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');

const app = express();
const routes = require('./routes');

require('dotenv').config();
const port = process.env.PORT;

app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use('/', routes);

app.listen(port, () => {
	console.log(`Server started on port ${port}.`);
})

module.exports = app;